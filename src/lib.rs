mod util;

mod buffers;
pub use buffers::*;

mod descriptors;
pub use descriptors::*;

mod device;
pub use device::*;

mod frame_resources;
pub use frame_resources::*;

mod image;
pub use image::*;

mod instance;
pub use instance::*;

mod pipeline;
pub use pipeline::*;

mod shaders;
pub use shaders::*;

mod surface;
pub use surface::*;

// This is all just temporary so that we can match the original API and test if the new implemenations are
// working. Switching to dynamic rendering is the end goal, but I don't want to do that until I'm sure regular
// rendering is working.

use nalgebra_glm as glm;
use std::{error::Error, ffi::CString, fs::File, path::Path, rc::Rc};

pub struct PushConstants {
    pub m_matrix: glm::Mat4,
    pub m_texture_index: u64,
}

#[derive(Copy, Clone)]
pub struct Vertex {
    pub m_position: glm::Vec3,
    pub m_colour: glm::Vec3,
    pub m_normal: glm::Vec3,
    pub m_texture_coords: glm::Vec2,
}