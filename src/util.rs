use std::ffi::{CStr, CString};

/// Converts a CString slice to a Vec<*const i8> for use with Vulkan.
pub fn cstring_to_ptr_arr(slice: &[CString]) -> Vec<*const i8> {
    slice
        .iter()
        .map(|c_string| c_string.as_ptr())
        .collect::<Vec<_>>()
}

/// Converts a &CStr slice to a Vec<*const i8> for use with Vulkan.
pub fn cstr_to_ptr_arr(slice: &[&CStr]) -> Vec<*const i8> {
    slice.iter().map(|c_str| c_str.as_ptr()).collect::<Vec<_>>()
}
