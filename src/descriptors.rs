use std::{error::Error, rc::Rc, collections::HashMap, hash::Hasher, hash::Hash, ptr::slice_from_raw_parts, cell::RefCell};

use crate::*;

pub struct DescriptorManager {
    m_descriptor_pool: ash::vk::DescriptorPool,
    m_layout_cache: HashMap<DescriptorLayoutInfo, ash::vk::DescriptorSetLayout>,

    m_device: Rc<Device>,
}

impl DescriptorManager {
    pub fn new(device: Rc<Device>) -> Result<Self, Box<dyn Error>> {
        let descriptor_pool_sizes = [
            *ash::vk::DescriptorPoolSize::builder()
                .ty(ash::vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                .descriptor_count(1024),
        ];

        let descriptor_pool_info = ash::vk::DescriptorPoolCreateInfo::builder()
            .flags(ash::vk::DescriptorPoolCreateFlags::empty())
            .max_sets(1024)
            .pool_sizes(&descriptor_pool_sizes);

        let descriptor_pool = unsafe {
            device
            .get_device_handle()
            .create_descriptor_pool(&descriptor_pool_info, None)?
        };

        Ok(Self {
            m_descriptor_pool: descriptor_pool,
            m_layout_cache: HashMap::new(),
            m_device: device,
        })
    }

    pub fn create_set_layout(
        &mut self, set_layout_info: ash::vk::DescriptorSetLayoutCreateInfo
    ) -> Result<ash::vk::DescriptorSetLayout, Box<dyn Error>> {
        let mut layout_info = DescriptorLayoutInfo {
            m_bindings: Vec::new(),
        };

        let bindings = slice_from_raw_parts(
            set_layout_info.p_bindings,
            set_layout_info.binding_count.try_into().unwrap());

        for i in 0..set_layout_info.binding_count {
            // Doing this feels dangerous...
            layout_info.m_bindings.push(unsafe { &*bindings }[i as usize]);
        }

        let result = self.m_layout_cache.get(&layout_info);
        if result.is_some() {
            return Ok(*result.unwrap());
        } else {
            unsafe {
                let set_layout = self.m_device
                    .get_device_handle()
                    .create_descriptor_set_layout(&set_layout_info, None)?;

                self.m_layout_cache.insert(layout_info, set_layout);

                Ok(set_layout)
            }
        }
    }

    pub fn allocate(
        &self,
        set_layout: ash::vk::DescriptorSetLayout
    ) -> Result<ash::vk::DescriptorSet, Box<dyn Error>> {
        let set_layouts = [set_layout];

        let set_allocate_info = ash::vk::DescriptorSetAllocateInfo::builder()
            .descriptor_pool(self.m_descriptor_pool)
            .set_layouts(&set_layouts);

        let set = unsafe {
            self.m_device.get_device_handle().allocate_descriptor_sets(&set_allocate_info)?[0]
        };

        Ok(set)
    }
}

impl Drop for DescriptorManager {
    fn drop(&mut self) {
        unsafe {
            for set_layout in self.m_layout_cache.iter() {
                self.m_device
                    .get_device_handle()
                    .destroy_descriptor_set_layout(*set_layout.1, None);
            }

            self.m_device
                .get_device_handle()
                .destroy_descriptor_pool(self.m_descriptor_pool, None);
        }
    }
}

pub struct DescriptorLayoutInfo {
    m_bindings: Vec<ash::vk::DescriptorSetLayoutBinding>,
}

impl PartialEq for DescriptorLayoutInfo {
    fn eq(&self, other: &Self) -> bool {
        if self.m_bindings.len() != other.m_bindings.len() {
            return false;
        }

        for (i, binding) in self.m_bindings.iter().enumerate() {
            if binding.binding != other.m_bindings[i].binding { return false; }
            if binding.descriptor_count != other.m_bindings[i].descriptor_count { return false; }
            if binding.descriptor_type != other.m_bindings[i].descriptor_type { return false; }
            if binding.stage_flags != other.m_bindings[i].stage_flags { return false; }
        }

        return true;
    }
}

impl Eq for DescriptorLayoutInfo {

}

impl Hash for DescriptorLayoutInfo {
    fn hash<H: Hasher>(&self, state: &mut H) {
        for binding in self.m_bindings.iter() {
            binding.binding.hash(state);
            binding.descriptor_count.hash(state);
            binding.descriptor_type.hash(state);
            binding.stage_flags.hash(state);
        }
    }
}

pub struct DescriptorBuilder {
    m_bindings: Vec<ash::vk::DescriptorSetLayoutBinding>,
    m_writes: Vec<ash::vk::WriteDescriptorSet>,
}

impl DescriptorBuilder {
    pub fn new() -> Self {
        Self {
            m_bindings: Vec::new(),
            m_writes: Vec::new(),
        }
    }

    pub fn bind_images(
        mut self,
        binding: u32,
        image_infos: &[ash::vk::DescriptorImageInfo],
        descriptor_type: ash::vk::DescriptorType,
        stage_flags: ash::vk::ShaderStageFlags,
    ) -> Self {
        self.m_bindings.push(ash::vk::DescriptorSetLayoutBinding::builder()
            .descriptor_type(descriptor_type)
            .descriptor_count(1024)
            .binding(binding)
            .stage_flags(stage_flags)
            .build());

        self.m_writes.push(ash::vk::WriteDescriptorSet::builder()
            .dst_binding(binding)
            // Leaving the destination set out until DescriptorBuilder::build().
            //.dst_set(dst_set)
            .descriptor_type(descriptor_type)
            .image_info(image_infos)
            .build());

        self
    }

    pub fn build(
        &mut self,
        descriptor_manager: Rc<RefCell<DescriptorManager>>,
        device: Rc<Device>
    ) -> Result<ash::vk::DescriptorSet, Box<dyn Error>> {
        let set_layout_info = ash::vk::DescriptorSetLayoutCreateInfo::builder()
            .flags(ash::vk::DescriptorSetLayoutCreateFlags::empty())
            .bindings(self.m_bindings.as_slice())
            .build();

        let set_layout = descriptor_manager.borrow_mut().create_set_layout(set_layout_info)?;

        let set = descriptor_manager.borrow_mut().allocate(set_layout)?;

        for write in self.m_writes.iter_mut() {
            write.dst_set = set;
        }

        println!("Building descriptor set with {} writes.", self.m_writes.len());

        unsafe {
            device.get_device_handle().update_descriptor_sets(
                self.m_writes.as_slice(),
                &[]
            );
        }

        Ok(set)
    }
}