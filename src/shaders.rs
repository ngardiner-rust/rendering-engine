use std::borrow::BorrowMut;
use std::cell::RefCell;
use std::error::Error;
use std::rc::Rc;

use spirv_reflect::types::*;
use ash::vk::Format as AshFormat;
use ash::vk::ShaderStageFlags as AshShaderStage;
use ash::vk::DescriptorType as AshDescriptorType;

use crate::*;

pub struct Shader {
    m_module: ash::vk::ShaderModule,
    m_bytecode: Vec<u32>,
    m_push_constant_ranges: Vec<ash::vk::PushConstantRange>,
    m_descriptor_set_layouts: Vec<ash::vk::DescriptorSetLayout>,
    m_stage_flags: ash::vk::ShaderStageFlags,

    // Empty unless it's a vertex shader.
    m_vertex_input_binding_descriptions: Vec<ash::vk::VertexInputBindingDescription>,
    m_vertex_input_attribute_descriptions: Vec<ash::vk::VertexInputAttributeDescription>,

    m_device: Rc<Device>,
}

impl Shader {
    // Load the compiled shader, perform reflection etc.
    pub fn load(
        path: &Path,
        device: Rc<Device>,
        descriptor_manager: Rc<RefCell<DescriptorManager>>,
    ) -> Result<Self, Box<dyn Error>> {
        let mut file = File::open(path)?;
        let bytecode = ash::util::read_spv(&mut file)?;

        let module_info = ash::vk::ShaderModuleCreateInfo::builder()
            .code(&bytecode);

        let module = unsafe {
            device.get_device_handle().create_shader_module(&module_info, None)?
        };

        let mut push_constant_ranges: Vec<ash::vk::PushConstantRange> = Vec::new();
        let mut descriptor_set_layouts: Vec<ash::vk::DescriptorSetLayout> = Vec::new();

        let reflection_module = spirv_reflect::ShaderModule::load_u32_data(&bytecode)?;

        let stage_flags = convert_shader_stage_flags(reflection_module.get_shader_stage());

        // Read the shader's push constant ranges.
        for push_constant_block in reflection_module.enumerate_push_constant_blocks(None)? {
            push_constant_ranges.push(ash::vk::PushConstantRange::builder()
                .offset(push_constant_block.offset)
                .size(push_constant_block.size)
                .stage_flags(stage_flags)
                .build());
        }

        // Read the shader's descriptor set layouts.
        for descriptor_set in reflection_module.enumerate_descriptor_sets(None)? {
            let mut set_bindings: Vec<ash::vk::DescriptorSetLayoutBinding> = Vec::new();

            for set_binding in descriptor_set.bindings {
                set_bindings.push(ash::vk::DescriptorSetLayoutBinding::builder()
                    .descriptor_type(convert_descriptor_type(set_binding.descriptor_type))
                    .descriptor_count(1024)
                    .binding(set_binding.binding)
                    .stage_flags(stage_flags)
                    .build()
                );
            }

            let set_layout_info = ash::vk::DescriptorSetLayoutCreateInfo::builder()
                .flags(ash::vk::DescriptorSetLayoutCreateFlags::empty())
                .bindings(set_bindings.as_slice())
                .build();

            // This will try to find the set layout in the cache first, to avoid duplicating it.
            descriptor_set_layouts.push(descriptor_manager.as_ref().borrow_mut().create_set_layout(set_layout_info)?)
        }

        let mut binding_descriptions: Vec<ash::vk::VertexInputBindingDescription> = Vec::new();
        let mut attribute_descriptions: Vec<ash::vk::VertexInputAttributeDescription> = Vec::new();

        if stage_flags.contains(ash::vk::ShaderStageFlags::VERTEX) {
            let mut binding_description = ash::vk::VertexInputBindingDescription::builder()
                .binding(0)
                .stride(0)
                .input_rate(ash::vk::VertexInputRate::VERTEX)
                .build();

            for input in reflection_module.enumerate_input_variables(None)? {
                let (format, format_size) = convert_format(input.format);
                attribute_descriptions.push(ash::vk::VertexInputAttributeDescription::builder()
                    .binding(0)
                    .location(input.location)
                    .format(format)
                    // Store the size of the format for the final offset calculation later.
                    .offset(format_size)
                    .build());
            }

            // Sort by location, and calculate the correct offsets.
            attribute_descriptions.sort_by(|a, b| {
                a.location.partial_cmp(&b.location).unwrap()
            });

            for attribute_description in attribute_descriptions.iter_mut() {
                let format_size = attribute_description.offset;
                attribute_description.offset = binding_description.stride;
                binding_description.stride += format_size;
            }

            binding_descriptions.push(binding_description);
        }

        Ok(Self {
            m_module: module,
            m_bytecode: bytecode,
            m_push_constant_ranges: push_constant_ranges,
            m_descriptor_set_layouts: descriptor_set_layouts,
            m_stage_flags: stage_flags,
            m_vertex_input_binding_descriptions: binding_descriptions,
            m_vertex_input_attribute_descriptions: attribute_descriptions,

            m_device: device,
        })
    }

    pub fn get_module(&self) -> &ash::vk::ShaderModule {
        &self.m_module
    }

    pub fn get_bytecode(&self) -> &Vec<u32> {
        &self.m_bytecode
    }

    pub fn get_push_constant_ranges(&self) -> &Vec<ash::vk::PushConstantRange> {
        &self.m_push_constant_ranges
    }

    pub fn get_descriptor_set_layouts(&self) -> &Vec<ash::vk::DescriptorSetLayout> {
        &self.m_descriptor_set_layouts
    }

    pub fn get_stage_flags(&self) -> ash::vk::ShaderStageFlags {
        self.m_stage_flags
    }

    pub fn get_vertex_input_binding_descriptions(&self) -> &Vec<ash::vk::VertexInputBindingDescription> {
        &self.m_vertex_input_binding_descriptions
    }

    pub fn get_vertex_input_attribute_descriptions(&self) -> &Vec<ash::vk::VertexInputAttributeDescription> {
        &self.m_vertex_input_attribute_descriptions
    }
}

impl Drop for Shader {
    fn drop(&mut self) {
        unsafe {
            self.m_device.get_device_handle().destroy_shader_module(self.m_module, None);
        }
    }
}

pub fn generate_pipeline_layout(
    device: Rc<Device>,
    vertex_shader: Vec<u32>,
    fragment_shader: Vec<u32>,
    descriptor_manager: &mut DescriptorManager
) -> Result<ash::vk::PipelineLayout, Box<dyn Error>> {
    let mut push_constant_ranges: Vec<ash::vk::PushConstantRange> = Vec::new();
    let mut set_layouts: Vec<ash::vk::DescriptorSetLayout> = Vec::new();

    for shader in vec![vertex_shader, fragment_shader].iter() {
        let module = spirv_reflect::ShaderModule::load_u32_data(shader)?;

        // Read the shader's push constant ranges.
        for push_constant_block in module.enumerate_push_constant_blocks(None)? {
            push_constant_ranges.push(ash::vk::PushConstantRange::builder()
                .offset(push_constant_block.offset)
                .size(push_constant_block.size)
                .stage_flags(convert_shader_stage_flags(module.get_shader_stage()))
                .build());
        }

        // Read the shader's descriptor set layouts.
        for descriptor_set in module.enumerate_descriptor_sets(None)? {
            let mut set_bindings: Vec<ash::vk::DescriptorSetLayoutBinding> = Vec::new();

            for set_binding in descriptor_set.bindings {
                set_bindings.push(ash::vk::DescriptorSetLayoutBinding::builder()
                    .descriptor_type(convert_descriptor_type(set_binding.descriptor_type))
                    .descriptor_count(set_binding.count)
                    .binding(set_binding.binding)
                    .stage_flags(convert_shader_stage_flags(module.get_shader_stage()))
                    .build()
                );
            }

            let set_layout_info = ash::vk::DescriptorSetLayoutCreateInfo::builder()
                .flags(ash::vk::DescriptorSetLayoutCreateFlags::empty())
                .bindings(set_bindings.as_slice())
                .build();

            // This will try to find the set layout in the cache first, to avoid duplicating it.
            set_layouts.push(descriptor_manager.create_set_layout(set_layout_info)?)
        }
    }

    // Create and return the pipeline layout.
    let pipeline_layout_info = ash::vk::PipelineLayoutCreateInfo::builder()
        .push_constant_ranges(push_constant_ranges.as_slice())
        .set_layouts(set_layouts.as_slice());

    unsafe {
        Ok(device.get_device_handle().create_pipeline_layout(&pipeline_layout_info, None)?)
    }
}

fn convert_format(reflect_format: ReflectFormat) -> (ash::vk::Format, u32) {
    match reflect_format {
        ReflectFormat::R32G32B32A32_SFLOAT => (AshFormat::R32G32B32A32_SFLOAT, 16),
        ReflectFormat::R32G32B32A32_SINT => (AshFormat::R32G32B32A32_SINT, 16),
        ReflectFormat::R32G32B32A32_UINT => (AshFormat::R32G32B32A32_UINT, 16),
        ReflectFormat::R32G32B32_SFLOAT => (AshFormat::R32G32B32_SFLOAT, 12),
        ReflectFormat::R32G32B32_SINT => (AshFormat::R32G32B32_SINT, 12),
        ReflectFormat::R32G32B32_UINT => (AshFormat::R32G32B32_UINT, 12),
        ReflectFormat::R32G32_SFLOAT => (AshFormat::R32G32_SFLOAT, 8),
        ReflectFormat::R32G32_SINT => (AshFormat::R32G32_SINT, 8),
        ReflectFormat::R32G32_UINT => (AshFormat::R32G32_UINT, 8),
        ReflectFormat::R32_SFLOAT => (AshFormat::R32_SFLOAT, 4),
        ReflectFormat::R32_SINT => (AshFormat::R32_SINT, 4),
        ReflectFormat::R32_UINT => (AshFormat::R32_UINT, 4),
        ReflectFormat::Undefined => (AshFormat::UNDEFINED, 0)
    }
}

fn convert_shader_stage_flags(reflect_stage: ReflectShaderStageFlags) -> AshShaderStage {
    let mut shader_stage_flags = AshShaderStage::empty();

    if reflect_stage.contains(ReflectShaderStageFlags::ANY_HIT_BIT_NV) {
        shader_stage_flags |= AshShaderStage::ANY_HIT_NV;
    }
    if reflect_stage.contains(ReflectShaderStageFlags::CALLABLE_BIT_NV) {
        shader_stage_flags |= AshShaderStage::CALLABLE_NV;
    }
    if reflect_stage.contains(ReflectShaderStageFlags::CLOSEST_HIT_BIT_NV) {
        shader_stage_flags |= AshShaderStage::CLOSEST_HIT_NV;
    }
    if reflect_stage.contains(ReflectShaderStageFlags::COMPUTE) {
        shader_stage_flags |= AshShaderStage::COMPUTE;
    }
    if reflect_stage.contains(ReflectShaderStageFlags::FRAGMENT) {
        shader_stage_flags |= AshShaderStage::FRAGMENT;
    }
    if reflect_stage.contains(ReflectShaderStageFlags::GEOMETRY) {
        shader_stage_flags |= AshShaderStage::GEOMETRY;
    }
    if reflect_stage.contains(ReflectShaderStageFlags::INTERSECTION_BIT_NV) {
        shader_stage_flags |= AshShaderStage::INTERSECTION_NV;
    }
    if reflect_stage.contains(ReflectShaderStageFlags::MISS_BIT_NV) {
        shader_stage_flags |= AshShaderStage::MISS_NV;
    }
    if reflect_stage.contains(ReflectShaderStageFlags::RAYGEN_BIT_NV) {
        shader_stage_flags |= AshShaderStage::RAYGEN_NV;
    }
    if reflect_stage.contains(ReflectShaderStageFlags::TESSELLATION_CONTROL) {
        shader_stage_flags |= AshShaderStage::TESSELLATION_CONTROL;
    }
    if reflect_stage.contains(ReflectShaderStageFlags::TESSELLATION_EVALUATION) {
        shader_stage_flags |= AshShaderStage::TESSELLATION_EVALUATION;
    }
    if reflect_stage.contains(ReflectShaderStageFlags::UNDEFINED) {
        shader_stage_flags |= AshShaderStage::empty();
    }
    if reflect_stage.contains(ReflectShaderStageFlags::VERTEX) {
        shader_stage_flags |= AshShaderStage::VERTEX;
    }

    shader_stage_flags
}

fn convert_descriptor_type(reflect_descriptor_type: ReflectDescriptorType) -> AshDescriptorType {
    match reflect_descriptor_type {
        ReflectDescriptorType::AccelerationStructureNV => AshDescriptorType::ACCELERATION_STRUCTURE_NV,
        ReflectDescriptorType::CombinedImageSampler => AshDescriptorType::COMBINED_IMAGE_SAMPLER,
        ReflectDescriptorType::InputAttachment => AshDescriptorType::INPUT_ATTACHMENT,
        ReflectDescriptorType::SampledImage => AshDescriptorType::SAMPLED_IMAGE,
        ReflectDescriptorType::Sampler => AshDescriptorType::SAMPLER,
        ReflectDescriptorType::StorageBuffer => AshDescriptorType::STORAGE_BUFFER,
        ReflectDescriptorType::StorageBufferDynamic => AshDescriptorType::UNIFORM_BUFFER_DYNAMIC,
        ReflectDescriptorType::StorageImage => AshDescriptorType::STORAGE_IMAGE,
        ReflectDescriptorType::StorageTexelBuffer => AshDescriptorType::STORAGE_TEXEL_BUFFER,
        ReflectDescriptorType::Undefined => AshDescriptorType::default(),
        ReflectDescriptorType::UniformBuffer => AshDescriptorType::UNIFORM_BUFFER,
        ReflectDescriptorType::UniformBufferDynamic => AshDescriptorType::UNIFORM_BUFFER_DYNAMIC,
        ReflectDescriptorType::UniformTexelBuffer => AshDescriptorType::UNIFORM_TEXEL_BUFFER
    }
}