use std::error::Error;
use std::ffi::CStr;
use std::sync::Arc;
use std::sync::Mutex;

use crate::util;
use crate::Instance;
use crate::Surface;

pub struct Device {
    m_physical_device_handle: ash::vk::PhysicalDevice,
    m_physical_device_properties: ash::vk::PhysicalDeviceProperties,
    m_graphics_family_index: u32,

    m_device_handle: ash::Device,
    m_graphics_queue: ash::vk::Queue,

    // This is only an Option so we can drop it in the destructor. It will always be initialised before drop().
    m_memory_allocator: Option<Arc<Mutex<gpu_allocator::vulkan::Allocator>>>,

    // Immediate submission resources.
    m_command_pool: ash::vk::CommandPool,
    m_command_buffer: ash::vk::CommandBuffer,
    m_completion_fence: ash::vk::Fence,
}

impl Device {
    pub fn new(instance: &Instance, surface: &Surface, discrete_only: bool) -> Result<Self, Box<dyn Error>> {
        let available_physical_devices = unsafe {
            instance
                .get_instance_handle()
                .enumerate_physical_devices()?
        };

        let mut physical_device: Option<ash::vk::PhysicalDevice> = None;
        let mut graphics_family_index: Option<u32> = None;

        'devices: for device in available_physical_devices.iter() {
            let device_queue_families = unsafe {
                instance
                    .get_instance_handle()
                    .get_physical_device_queue_family_properties(*device)
            };

            let device_properties = unsafe {
                instance
                    .get_instance_handle()
                    .get_physical_device_properties(*device)
            };

            '_families: for (queue_family_index, properties) in
                device_queue_families.iter().enumerate()
            {
                if surface
                    .get_presentation_support(device, queue_family_index.try_into().unwrap())
                    .unwrap()
                    && properties
                        .queue_flags
                        .contains(ash::vk::QueueFlags::GRAPHICS)
                    && (!discrete_only || device_properties.device_type == ash::vk::PhysicalDeviceType::DISCRETE_GPU)
                {
                    physical_device = Some(*device);
                    graphics_family_index = Some(queue_family_index as u32);

                    break 'devices;
                }
            }
        }

        if physical_device.is_none() {
            return Err(String::from("No suitable devices found.").into());
        }

        let physical_device_properties = unsafe {
            instance
                .get_instance_handle()
                .get_physical_device_properties(physical_device.unwrap())
        };

        let queue_infos = [ash::vk::DeviceQueueCreateInfo::builder()
            .queue_family_index(graphics_family_index.unwrap())
            .queue_priorities(&[1.0])
            .build()];

        let mut dynamic_rendering_features = ash::vk::PhysicalDeviceDynamicRenderingFeaturesKHR::builder()
            .dynamic_rendering(true);

        let mut descriptor_indexing_features = ash::vk::PhysicalDeviceDescriptorIndexingFeatures::builder()
            .descriptor_binding_partially_bound(true)
            .runtime_descriptor_array(true);

        let mut required_device_features = *ash::vk::PhysicalDeviceFeatures2::builder()
            .push_next(&mut dynamic_rendering_features)
            .push_next(&mut descriptor_indexing_features)
            // Enable any standard device features here.
            .features(*ash::vk::PhysicalDeviceFeatures::builder());

        let required_device_extensions = [
            ash::extensions::khr::Swapchain::name(),
            ash::extensions::khr::DynamicRendering::name(),
        ];

        let supported_device_extensions = unsafe {
            instance
                .get_instance_handle()
                .enumerate_device_extension_properties(physical_device.unwrap())?
                .iter()
                .map(|supported_device_extension| {
                    CStr::from_ptr(supported_device_extension.extension_name.as_ptr())
                })
                .collect::<Vec<_>>()
        };

        for required_device_extension in required_device_extensions.iter() {
            if !supported_device_extensions.contains(required_device_extension) {
                return Err(format!(
                    "The device extension {:?} was requested but is not supported.",
                    required_device_extension
                )
                .into());
            }
        }

        let converted_device_extension_names = util::cstr_to_ptr_arr(&required_device_extensions);

        let device_info = ash::vk::DeviceCreateInfo::builder()
            .push_next(&mut required_device_features)
            .queue_create_infos(&queue_infos)
            .enabled_extension_names(&converted_device_extension_names);

        let device = unsafe {
            instance.get_instance_handle().create_device(
                physical_device.unwrap(),
                &device_info,
                None,
            )?
        };

        log::trace!("Initialised device.");

        let graphics_queue = unsafe { device.get_device_queue(graphics_family_index.unwrap(), 0) };

        let memory_allocator_info = gpu_allocator::vulkan::AllocatorCreateDesc {
            instance: instance.get_instance_handle().clone(),
            physical_device: physical_device.unwrap(),
            device: device.clone(),
            debug_settings: Default::default(),
            buffer_device_address: false,
        };

        let memory_allocator = Arc::new(Mutex::new(gpu_allocator::vulkan::Allocator::new(
            &memory_allocator_info,
        )?));

        // Initialise immediate submission resources.
        let command_pool_info = ash::vk::CommandPoolCreateInfo::builder()
            .queue_family_index(graphics_family_index.unwrap())
            .flags(ash::vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER);

        let command_pool = unsafe {
            device
                .create_command_pool(&command_pool_info, None)?
        };

        let command_buffer_info = ash::vk::CommandBufferAllocateInfo::builder()
            .command_pool(command_pool)
            .level(ash::vk::CommandBufferLevel::PRIMARY)
            .command_buffer_count(1);

        let command_buffer = unsafe {
            device
                .allocate_command_buffers(&command_buffer_info)?[0]
        };

        let completion_fence = unsafe {
            device.create_fence(&ash::vk::FenceCreateInfo::default(), None)?
        };

        Ok(Self {
            m_physical_device_handle: physical_device.unwrap(),
            m_physical_device_properties: physical_device_properties,
            m_graphics_family_index: graphics_family_index.unwrap(),

            m_device_handle: device,
            m_graphics_queue: graphics_queue,

            m_memory_allocator: Some(memory_allocator),

            m_command_pool: command_pool,
            m_command_buffer: command_buffer,
            m_completion_fence: completion_fence,
        })
    }

    pub fn get_physical_device_handle(&self) -> &ash::vk::PhysicalDevice {
        &self.m_physical_device_handle
    }

    pub fn get_physical_device_properties(&self) -> &ash::vk::PhysicalDeviceProperties {
        &self.m_physical_device_properties
    }

    pub fn get_graphics_family_index(&self) -> u32 {
        self.m_graphics_family_index
    }

    pub fn get_device_handle(&self) -> &ash::Device {
        &self.m_device_handle
    }

    pub fn get_graphics_queue(&self) -> &ash::vk::Queue {
        &self.m_graphics_queue
    }

    pub fn get_memory_allocator(&self) -> Arc<Mutex<gpu_allocator::vulkan::Allocator>> {
        self.m_memory_allocator.as_ref().unwrap().clone()
    }

    pub fn get_command_pool(&self) -> &ash::vk::CommandPool {
        &self.m_command_pool
    }

    pub fn perform_immediate_submission<F>(
        &self,
        function: F
    ) -> Result<(), Box<dyn Error>> where F: Fn(ash::vk::CommandBuffer) {
        let command_buffer_begin_info = ash::vk::CommandBufferBeginInfo::builder()
            .flags(ash::vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);

        unsafe {
            self.m_device_handle.begin_command_buffer(self.m_command_buffer, &command_buffer_begin_info)?;

            function(self.m_command_buffer);

            self.m_device_handle.end_command_buffer(self.m_command_buffer)?;

            let submission_command_buffers = [self.m_command_buffer];

            let submission_info = ash::vk::SubmitInfo::builder()
                .command_buffers(&submission_command_buffers);

            self.m_device_handle.queue_submit(
                self.m_graphics_queue, &[*submission_info], self.m_completion_fence)?;

            // Wait for the command buffer to be executed and reset the command pool for the next use.
            self.m_device_handle.wait_for_fences(&[self.m_completion_fence], true, std::u64::MAX)?;
            self.m_device_handle.reset_fences(&[self.m_completion_fence])?;

            self.m_device_handle.reset_command_pool(self.m_command_pool, ash::vk::CommandPoolResetFlags::empty())?;
        }

        Ok(())
    }

    pub fn perform_image_layout_transition(
        &self,
        command_buffer: &ash::vk::CommandBuffer,
        image: ash::vk::Image,
        subresource_range: ash::vk::ImageSubresourceRange,
        src_access_mask: ash::vk::AccessFlags,
        dst_access_mask: ash::vk::AccessFlags,
        old_layout: ash::vk::ImageLayout,
        new_layout: ash::vk::ImageLayout,
        src_stage_mask: ash::vk::PipelineStageFlags,
        dst_stage_mask: ash::vk::PipelineStageFlags
    ) -> Result<(), Box<dyn Error>> {
        let image_memory_barrier = ash::vk::ImageMemoryBarrier::builder()
            .src_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED)
            .dst_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED)
            .src_access_mask(src_access_mask)
            .dst_access_mask(dst_access_mask)
            .old_layout(old_layout)
            .new_layout(new_layout)
            .image(image)
            .subresource_range(subresource_range);

        unsafe {
            self.get_device_handle().cmd_pipeline_barrier(
                *command_buffer,
                src_stage_mask,
                dst_stage_mask,
                ash::vk::DependencyFlags::empty(),
                &[],
                &[],
                &[*image_memory_barrier]
            );
        }

        Ok(())
    }
}

impl Drop for Device {
    fn drop(&mut self) {
        unsafe {
            // The allocator needs to be dropped before the device.
            std::mem::take(&mut self.m_memory_allocator);

            // Destroy the immediate submission resources.
            self.m_device_handle.destroy_fence(self.m_completion_fence, None);
            self.m_device_handle.destroy_command_pool(self.m_command_pool, None);

            self.m_device_handle.destroy_device(None);
            log::trace!("Dropped device.")
        }
    }
}