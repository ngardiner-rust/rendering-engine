use std::{rc::Rc};

use crate::*;

pub struct Buffer<T> {
    m_buffer_handle: ash::vk::Buffer,
    m_memory_requirements: ash::vk::MemoryRequirements,
    m_allocation: gpu_allocator::vulkan::Allocation,
    m_data: Vec<T>,

    m_device: Rc<Device>,
}

impl<T> Buffer<T> {
    pub fn new(
        device: Rc<Device>,
        size: ash::vk::DeviceSize,
        usage: ash::vk::BufferUsageFlags,
        location: gpu_allocator::MemoryLocation,
    ) -> Result<Self, Box<dyn Error>> {
        let buffer_info = ash::vk::BufferCreateInfo::builder()
            .size(size)
            .usage(usage)
            .sharing_mode(ash::vk::SharingMode::EXCLUSIVE);

        let buffer = unsafe {
            device
                .get_device_handle()
                .create_buffer(&buffer_info, None)?
        };

        let memory_requirements = unsafe {
            device
                .get_device_handle()
                .get_buffer_memory_requirements(buffer)
        };

        let allocation_info = gpu_allocator::vulkan::AllocationCreateDesc {
            name: "Buffer",
            requirements: memory_requirements,
            location,
            linear: false,
        };

        let allocation = device
            .get_memory_allocator()
            .lock().unwrap()
            .allocate(&allocation_info)?;

        unsafe {
            device.get_device_handle().bind_buffer_memory(
                buffer,
                allocation.memory(),
                allocation.offset(),
            )?
        };

        Ok(Self {
            m_buffer_handle: buffer,
            m_memory_requirements: memory_requirements,
            m_allocation: allocation,
            m_data: Vec::new(),
            m_device: device,
        })
    }

    pub fn new_populated(
        device: Rc<Device>,
        usage: ash::vk::BufferUsageFlags,
        data: Vec<T>
    ) -> Result<Self, Box<dyn Error>> {
        let data_size = (data.len() * std::mem::size_of::<T>()).try_into().unwrap();

        let staging_buffer: Buffer<T> = Buffer::new(
            device.clone(),
            data_size,
            ash::vk::BufferUsageFlags::TRANSFER_SRC,
            // This memory location is the closest type to VMA's CPU-only memory.
            gpu_allocator::MemoryLocation::GpuToCpu
        )?;

        unsafe {
            // No need to map first or unmap after, as gpu_allocator maps the memory when allocating.
            let memory_pointer = staging_buffer.get_allocation().mapped_ptr().unwrap().as_ptr() as *mut T;
            memory_pointer.copy_from_nonoverlapping(data.as_ptr(), data.len());
        }

        let mut destination_buffer = Buffer::new(
            device.clone(),
            data_size,
            usage | ash::vk::BufferUsageFlags::TRANSFER_DST,
            gpu_allocator::MemoryLocation::GpuOnly
        )?;

        unsafe {
            device.perform_immediate_submission(|command_buffer| {
                let buffer_region = ash::vk::BufferCopy::builder()
                    .size(data_size)
                    .build();

                device.get_device_handle().cmd_copy_buffer(
                    command_buffer, 
                    *staging_buffer.get_buffer_handle(),
                    *destination_buffer.get_buffer_handle(),
                    &[buffer_region]);
            })?;
        }

        destination_buffer.m_data = data;

        Ok(destination_buffer)
    }

    pub fn get_buffer_handle(&self) -> &ash::vk::Buffer {
        &self.m_buffer_handle
    }

    pub fn get_allocation(&self) -> &gpu_allocator::vulkan::Allocation {
        &self.m_allocation
    }

    pub fn get_data(&self) -> &Vec<T> {
        &self.m_data
    }
}

impl<T> Drop for Buffer<T> {
    fn drop(&mut self) {
        unsafe {
            self.m_device
                .get_memory_allocator()
                .lock().unwrap()
                .free(std::mem::take(&mut self.m_allocation)).unwrap();

            self.m_device
                .get_device_handle()
                .destroy_buffer(self.m_buffer_handle, None);
        }
    }
}
