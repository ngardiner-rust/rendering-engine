use std::{error::Error, path, io::Read, cell::RefCell};

use crate::*;

pub struct Image {
    m_image_handle: ash::vk::Image,
    m_memory_requirements: ash::vk::MemoryRequirements,
    m_allocation: gpu_allocator::vulkan::Allocation,

    m_view: ash::vk::ImageView,
    m_subresource_range: ash::vk::ImageSubresourceRange,

    m_device: Rc<Device>,
}

impl Image {
    pub fn new(
        device: Rc<Device>,
        extent: ash::vk::Extent3D,
        format: ash::vk::Format,
        usage: ash::vk::ImageUsageFlags,
        aspect_mask: ash::vk::ImageAspectFlags,
        mip_levels: u32,
    ) -> Result<Self, Box<dyn Error>> {
        let image_info = ash::vk::ImageCreateInfo::builder()
            .flags(ash::vk::ImageCreateFlags::empty())
            .image_type(ash::vk::ImageType::TYPE_2D)
            .format(format)
            .extent(extent)
            .mip_levels(mip_levels)
            .array_layers(1)
            .samples(ash::vk::SampleCountFlags::TYPE_1)
            .tiling(ash::vk::ImageTiling::OPTIMAL)
            .usage(usage)
            .sharing_mode(ash::vk::SharingMode::EXCLUSIVE)
            .initial_layout(ash::vk::ImageLayout::UNDEFINED);

        let image = unsafe {
            device
                .get_device_handle()
                .create_image(&image_info, None)?
        };
    
        let memory_requirements = unsafe {
            device
                .get_device_handle()
                .get_image_memory_requirements(image)
        };

        let allocation_info = gpu_allocator::vulkan::AllocationCreateDesc {
            name: "Image",
            requirements: memory_requirements,
            location: gpu_allocator::MemoryLocation::GpuOnly,
            linear: false,
        };

        let allocation = device
            .get_memory_allocator()
            .lock().unwrap()
            .allocate(&allocation_info)?;

        unsafe {
            device.get_device_handle().bind_image_memory(
                image, 
                allocation.memory(), 
                allocation.offset(),
            )?
        }

        let subresource_range = ash::vk::ImageSubresourceRange::builder()
            .aspect_mask(aspect_mask)
            .base_mip_level(0)
            .level_count(mip_levels)
            .base_array_layer(0)
            .layer_count(1)
            .build();

        let image_view_info = ash::vk::ImageViewCreateInfo::builder()
            .flags(ash::vk::ImageViewCreateFlags::empty())
            .image(image)
            .view_type(ash::vk::ImageViewType::TYPE_2D)
            .format(format)
            .subresource_range(subresource_range);

        let image_view = unsafe {
            device.get_device_handle().create_image_view(&image_view_info, None)?
        };

        Ok(Self {
            m_image_handle: image,
            m_memory_requirements: memory_requirements,
            m_allocation: allocation,
            m_view: image_view,
            m_subresource_range: subresource_range,
            m_device: device
        })
    }

    pub fn get_image_handle(&self) -> &ash::vk::Image {
        &self.m_image_handle
    }

    pub fn get_view(&self) -> &ash::vk::ImageView {
        &self.m_view
    }

    pub fn get_subresource_range(&self) -> &ash::vk::ImageSubresourceRange {
        &self.m_subresource_range
    }
}

impl Drop for Image {
    fn drop(&mut self) {
        unsafe {
            self.m_device
                .get_device_handle()
                .destroy_image_view(self.m_view, None);
            
            self.m_device
                .get_memory_allocator()
                .lock().unwrap()
                .free(std::mem::take(&mut self.m_allocation))
                .unwrap();
            
            self.m_device
                .get_device_handle()
                .destroy_image(self.m_image_handle, None);
        }
    }
}

pub struct Texture {
    m_image: Image,
    m_sampler: ash::vk::Sampler,

    m_device: Rc<Device>,
}

impl Texture {
    pub fn new(
        device: Rc<Device>,
        path: &str,
        descriptor_manager: Rc<RefCell<DescriptorManager>>,
    ) -> Result<Self, Box<dyn Error>> {
        let mut file = File::open(path)?;
        let mut contents = vec![];
        file.read_to_end(&mut contents)?;

        let mut width: i32 = 0;
        let mut height: i32 = 0;
        // This gives the number of components the image would have had, not how many it's actually being loaded
        // with.
        let mut channels: i32 = 0;
        let image_data: *mut u8;

        unsafe {
            image_data = stb_image_rust::stbi_load_from_memory(
                contents.as_mut_ptr(),
                contents.len() as i32,
                &mut width,
                &mut height,
                &mut channels,
                stb_image_rust::STBI_rgb_alpha,
            );
        }

        let data_size: ash::vk::DeviceSize = (width * height * stb_image_rust::STBI_rgb_alpha).try_into().unwrap();

        let staging_buffer: Buffer<*mut u8> = Buffer::new(
            device.clone(),
            data_size,
            ash::vk::BufferUsageFlags::TRANSFER_SRC,
            gpu_allocator::MemoryLocation::GpuToCpu
        )?;

        unsafe {
            let memory_pointer = staging_buffer.get_allocation().mapped_ptr().unwrap().as_ptr() as *mut u8;
            memory_pointer.copy_from_nonoverlapping(image_data, data_size.try_into().unwrap());
            stb_image_rust::c_runtime::free(image_data);
        }

        let image_extent = ash::vk::Extent3D::builder()
            .width(width.try_into().unwrap())
            .height(height.try_into().unwrap())
            .depth(1)
            .build();

        let image = Image::new(
            device.clone(),
            image_extent,
            ash::vk::Format::R8G8B8A8_SRGB,
            ash::vk::ImageUsageFlags::SAMPLED | ash::vk::ImageUsageFlags::TRANSFER_DST,
            ash::vk::ImageAspectFlags::COLOR,
            1
        )?;

        device.perform_immediate_submission(|command_buffer| {
            // Transition the image for writing to it.
            device.perform_image_layout_transition(
                &command_buffer,
                *image.get_image_handle(),
                *image.get_subresource_range(),
                ash::vk::AccessFlags::empty(),
                ash::vk::AccessFlags::TRANSFER_WRITE,
                ash::vk::ImageLayout::UNDEFINED,
                ash::vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                ash::vk::PipelineStageFlags::TOP_OF_PIPE,
                ash::vk::PipelineStageFlags::TRANSFER
            ).unwrap();

            let buffer_region = ash::vk::BufferImageCopy::builder()
                .buffer_row_length(0)
                .buffer_image_height(0)
                .buffer_offset(0)
                .image_subresource(ash::vk::ImageSubresourceLayers::builder()
                    .aspect_mask(ash::vk::ImageAspectFlags::COLOR)
                    .mip_level(0)
                    .base_array_layer(0)
                    .layer_count(1)
                    .build())
                .image_extent(image_extent);

            // Write the data in the staging buffer to the image.
            unsafe {
                device.get_device_handle().cmd_copy_buffer_to_image(
                    command_buffer,
                    *staging_buffer.get_buffer_handle(),
                    *image.get_image_handle(),
                    ash::vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                    &[*buffer_region]);
            }

            // Transition the image for the shader to read from it.
            device.perform_image_layout_transition(
                &command_buffer,
                *image.get_image_handle(),
                *image.get_subresource_range(),
                ash::vk::AccessFlags::TRANSFER_WRITE,
                ash::vk::AccessFlags::SHADER_READ,
                ash::vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                ash::vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                ash::vk::PipelineStageFlags::TRANSFER,
                ash::vk::PipelineStageFlags::FRAGMENT_SHADER
            ).unwrap();
        })?;

        // Hardcode this here for now, but in the future we'll need something like a string-indexed hashmap
        // of the samplers we need.
        let sampler_info = ash::vk::SamplerCreateInfo::builder()
            .mag_filter(ash::vk::Filter::NEAREST)
            .min_filter(ash::vk::Filter::NEAREST)
            .address_mode_u(ash::vk::SamplerAddressMode::REPEAT)
            .address_mode_v(ash::vk::SamplerAddressMode::REPEAT)
            .address_mode_w(ash::vk::SamplerAddressMode::REPEAT);

        let sampler = unsafe {
            device
                .get_device_handle()
                .create_sampler(&sampler_info, None)?
        };

        Ok(Self {
            m_image: image,
            m_sampler: sampler,

            m_device: device,
        })
    }

    pub fn get_image(&self) -> &Image {
        &self.m_image
    }

    pub fn get_view(&self) -> &ash::vk::ImageView {
        &self.m_image.get_view()
    }

    pub fn get_sampler(&self) -> &ash::vk::Sampler {
        &self.m_sampler
    }
}

impl Drop for Texture {
    fn drop(&mut self) {
        unsafe {
            self.m_device.get_device_handle().destroy_sampler(self.m_sampler, None);
        }
    }
}