use std::error::Error;
use std::rc::Rc;

use winit::window::Window;

use crate::Device;
use crate::Instance;

pub struct Surface {
    m_surface_handle: ash::vk::SurfaceKHR,
    m_surface_loader: ash::extensions::khr::Surface,

    m_current_swapchain: Option<Swapchain>,
}

impl Surface {
    pub fn new(window: &Window, instance: &Instance) -> Result<Self, Box<dyn Error>> {
        let surface_loader = ash::extensions::khr::Surface::new(
            instance.get_entry(),
            instance.get_instance_handle(),
        );

        let surface = unsafe {
            ash_window::create_surface(
                instance.get_entry(),
                instance.get_instance_handle(),
                window,
                None,
            )?
        };

        log::trace!("Initialised surface.");

        Ok(Self {
            m_surface_handle: surface,
            m_surface_loader: surface_loader,

            m_current_swapchain: None,
        })
    }

    pub fn get_surface_handle(&self) -> &ash::vk::SurfaceKHR {
        &self.m_surface_handle
    }

    fn get_surface_loader(&self) -> &ash::extensions::khr::Surface {
        &self.m_surface_loader
    }

    pub fn get_supported_formats(
        &self,
        physical_device: &ash::vk::PhysicalDevice,
    ) -> Result<Vec<ash::vk::SurfaceFormatKHR>, Box<dyn Error>> {
        unsafe {
            Ok(self
                .get_surface_loader()
                .get_physical_device_surface_formats(
                    *physical_device,
                    *self.get_surface_handle(),
                )?)
        }
    }

    pub fn get_supported_capabilities(
        &self,
        physical_device: &ash::vk::PhysicalDevice,
    ) -> Result<ash::vk::SurfaceCapabilitiesKHR, Box<dyn Error>> {
        unsafe {
            Ok(self
                .get_surface_loader()
                .get_physical_device_surface_capabilities(
                    *physical_device,
                    *self.get_surface_handle(),
                )?)
        }
    }

    pub fn get_supported_present_modes(
        &self,
        physical_device: &ash::vk::PhysicalDevice,
    ) -> Result<Vec<ash::vk::PresentModeKHR>, Box<dyn Error>> {
        unsafe {
            Ok(self
                .get_surface_loader()
                .get_physical_device_surface_present_modes(
                    *physical_device,
                    *self.get_surface_handle(),
                )?)
        }
    }

    pub fn get_presentation_support(
        &self,
        physical_device: &ash::vk::PhysicalDevice,
        presentation_queue_index: u32,
    ) -> Result<bool, Box<dyn Error>> {
        unsafe {
            Ok(self
                .get_surface_loader()
                .get_physical_device_surface_support(
                    *physical_device,
                    presentation_queue_index,
                    *self.get_surface_handle(),
                )?)
        }
    }

    pub fn initialise_swapchain(
        &mut self,
        window: &Window,
        instance: &Instance,
        device: Rc<Device>,
    ) -> Result<(), Box<dyn Error>> {
        self.m_current_swapchain = Some(Swapchain::new(window, instance, self, device, None)?);

        Ok(())
    }

    pub fn get_current_swapchain(&self) -> &Swapchain {
        self.m_current_swapchain.as_ref().unwrap()
    }

    pub fn get_current_swapchain_mut(&mut self) -> &mut Swapchain {
        self.m_current_swapchain.as_mut().unwrap()
    }

    pub fn get_current_swapchain_extent(&self) -> ash::vk::Extent2D {
        self.m_current_swapchain.as_ref().unwrap().get_extent()
    }

    // TODO: Remove this once we've tested that everything works as before.
    pub fn recreate_swapchain(
        &mut self,
        window: &Window,
        instance: &Instance,
        logical_device: Rc<Device>,
    ) -> Result<(u32, u32), Box<dyn Error>> {
        unsafe {
            logical_device
                .get_device_handle()
                .device_wait_idle()
                .unwrap();
        }

        let new_swapchain = Swapchain::new(
            window,
            instance,
            self,
            logical_device,
            self.m_current_swapchain.as_ref(),
        )?;

        // If the format for the new swapchain is different, we need to also recreate
        // the graphics pipeline.
        if new_swapchain.get_image_format()
            != self
                .m_current_swapchain
                .as_ref()
                .unwrap()
                .get_image_format()
        {
            log::trace!("Format has changed.");
            // Recreate the pipeline.
        }

        let width = new_swapchain.get_extent().width;
        let height = new_swapchain.get_extent().height;

        // Destroy m_current_swapchain, and set it to the new swapchain.
        self.m_current_swapchain = Some(new_swapchain);

        Ok((width, height))
    }
}

impl Drop for Surface {
    fn drop(&mut self) {
        unsafe {
            self.m_current_swapchain = None;

            self.m_surface_loader
                .destroy_surface(self.m_surface_handle, None);
            log::trace!("Dropped surface.");
        }
    }
}

pub struct Swapchain {
    m_swapchain_handle: ash::vk::SwapchainKHR,
    m_swapchain_loader: ash::extensions::khr::Swapchain,

    m_image_format: ash::vk::SurfaceFormatKHR,
    m_extent: ash::vk::Extent2D,

    m_images: Vec<ash::vk::Image>,
    m_image_views: Vec<ash::vk::ImageView>,

    m_device: Rc<Device>,
}

impl Swapchain {
    pub fn new(
        window: &Window,
        instance: &Instance,
        surface: &Surface,
        device: Rc<Device>,
        old_swapchain: Option<&Swapchain>,
    ) -> Result<Self, Box<dyn Error>> {
        let swapchain_loader = ash::extensions::khr::Swapchain::new(
            instance.get_instance_handle(),
            device.get_device_handle(),
        );

        let supported_surface_formats =
            surface.get_supported_formats(device.get_physical_device_handle())?;

        let swapchain_image_format = *supported_surface_formats
            .iter()
            .find(|format| {
                format.format == ash::vk::Format::B8G8R8A8_SRGB
                    && format.color_space == ash::vk::ColorSpaceKHR::SRGB_NONLINEAR
            })
            .unwrap_or_else(|| &supported_surface_formats[0]);

        let supported_surface_capabilities =
            surface.get_supported_capabilities(device.get_physical_device_handle())?;

        let swapchain_extent = match supported_surface_capabilities.current_extent.width {
            std::u32::MAX => ash::vk::Extent2D::builder()
                .width(window.inner_size().width.clamp(
                    supported_surface_capabilities.min_image_extent.width,
                    supported_surface_capabilities.max_image_extent.width,
                ))
                .height(window.inner_size().height.clamp(
                    supported_surface_capabilities.min_image_extent.height,
                    supported_surface_capabilities.max_image_extent.height,
                ))
                .build(),
            _ => supported_surface_capabilities.current_extent,
        };

        let swapchain_image_count = if supported_surface_capabilities.max_image_count > 0
            && supported_surface_capabilities.min_image_count + 1
                > supported_surface_capabilities.max_image_count
        {
            supported_surface_capabilities.max_image_count
        } else {
            supported_surface_capabilities.min_image_count + 1
        };

        let swapchain_present_mode = *surface
            .get_supported_present_modes(device.get_physical_device_handle())
            .unwrap()
            .iter()
            .find(|mode| **mode == ash::vk::PresentModeKHR::FIFO)
            .unwrap_or(&ash::vk::PresentModeKHR::FIFO);

        let queue_family_indices = [device.get_graphics_family_index()];

        let swapchain_info = ash::vk::SwapchainCreateInfoKHR::builder()
            .flags(ash::vk::SwapchainCreateFlagsKHR::empty())
            .surface(*surface.get_surface_handle())
            .min_image_count(swapchain_image_count)
            .image_format(swapchain_image_format.format)
            .image_color_space(swapchain_image_format.color_space)
            .image_extent(swapchain_extent)
            .image_array_layers(1)
            .image_usage(ash::vk::ImageUsageFlags::COLOR_ATTACHMENT)
            .image_sharing_mode(ash::vk::SharingMode::EXCLUSIVE)
            .queue_family_indices(&queue_family_indices)
            .pre_transform(supported_surface_capabilities.current_transform)
            .composite_alpha(ash::vk::CompositeAlphaFlagsKHR::OPAQUE)
            .present_mode(swapchain_present_mode)
            .clipped(true)
            .old_swapchain(
                old_swapchain.map_or(ash::vk::SwapchainKHR::null(), |swapchain| {
                    *swapchain.get_swapchain_handle()
                }),
            );

        let swapchain = unsafe { swapchain_loader.create_swapchain(&swapchain_info, None)? };

        let swapchain_images = unsafe { swapchain_loader.get_swapchain_images(swapchain)? };

        let mut swapchain_image_views: Vec<ash::vk::ImageView> = Vec::new();
        for swapchain_image in swapchain_images.iter() {
            let image_view_info = ash::vk::ImageViewCreateInfo::builder()
                .image(*swapchain_image)
                .view_type(ash::vk::ImageViewType::TYPE_2D)
                .format(swapchain_image_format.format)
                .components(
                    *ash::vk::ComponentMapping::builder()
                        .r(ash::vk::ComponentSwizzle::IDENTITY)
                        .g(ash::vk::ComponentSwizzle::IDENTITY)
                        .b(ash::vk::ComponentSwizzle::IDENTITY)
                        .a(ash::vk::ComponentSwizzle::IDENTITY),
                )
                .subresource_range(
                    *ash::vk::ImageSubresourceRange::builder()
                        .aspect_mask(ash::vk::ImageAspectFlags::COLOR)
                        .base_mip_level(0)
                        .level_count(1)
                        .base_array_layer(0)
                        .layer_count(1),
                );

            let image_view = unsafe {
                device
                    .get_device_handle()
                    .create_image_view(&image_view_info, None)?
            };

            swapchain_image_views.push(image_view);
        }

        log::trace!("Initialised swapchain.");

        Ok(Self {
            m_swapchain_handle: swapchain,
            m_swapchain_loader: swapchain_loader,

            m_image_format: swapchain_image_format,
            m_extent: swapchain_extent,

            m_images: swapchain_images,
            m_image_views: swapchain_image_views,

            m_device: device,
        })
    }

    pub fn get_swapchain_handle(&self) -> &ash::vk::SwapchainKHR {
        &self.m_swapchain_handle
    }

    pub fn get_swapchain_loader(&self) -> &ash::extensions::khr::Swapchain {
        &self.m_swapchain_loader
    }

    pub fn get_image_format(&self) -> ash::vk::SurfaceFormatKHR {
        self.m_image_format
    }

    pub fn get_extent(&self) -> ash::vk::Extent2D {
        self.m_extent
    }

    pub fn get_images(&self) -> &Vec<ash::vk::Image> {
        &self.m_images
    }

    pub fn get_image_views(&self) -> &Vec<ash::vk::ImageView> {
        &self.m_image_views
    }
}

impl Drop for Swapchain {
    fn drop(&mut self) {
        unsafe {
            for swapchain_image_view in self.m_image_views.iter() {
                self.m_device
                    .get_device_handle()
                    .destroy_image_view(*swapchain_image_view, None);
            }

            self.m_swapchain_loader
                .destroy_swapchain(self.m_swapchain_handle, None);

            log::trace!("Dropped swapchain.");
        }
    }
}
