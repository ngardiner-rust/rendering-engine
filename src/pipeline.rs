use std::error::Error;

use crate::*;

pub struct PipelineBuilder {
    m_shaders: Vec<Rc<Shader>>,
    m_topology: ash::vk::PrimitiveTopology,
    m_depth_clamp_enable: bool,
    m_polygon_mode: ash::vk::PolygonMode,
    m_line_width: f32,
    m_cull_mode: ash::vk::CullModeFlags,
    m_front_face: ash::vk::FrontFace,
    m_colour_formats: Vec<ash::vk::Format>,
    m_depth_format: ash::vk::Format,
    m_stencil_format: ash::vk::Format,
}

impl PipelineBuilder {
    pub fn build(
        &self,
        device: Rc<Device>,
        viewport_state: &ViewportState,
    ) -> Result<Pipeline, Box<dyn Error>> {
        let mut shader_stage_infos: Vec<ash::vk::PipelineShaderStageCreateInfo> = Vec::new();
        let entry_name = CString::new("main").unwrap();

        let mut push_constant_ranges: Vec<ash::vk::PushConstantRange> = Vec::new();
        let mut descriptor_set_layouts: Vec<ash::vk::DescriptorSetLayout> = Vec::new();
        let mut vertex_binding_descriptions = &Vec::new();
        let mut vertex_attribute_descriptions = &Vec::new();

        for shader in self.m_shaders.iter() {
            shader_stage_infos.push(
                ash::vk::PipelineShaderStageCreateInfo::builder()
                    .module(*shader.get_module())
                    .stage(shader.get_stage_flags())
                    .name(entry_name.as_c_str())
                    .build(),
            );

            if shader
                .get_stage_flags()
                .contains(ash::vk::ShaderStageFlags::VERTEX)
            {
                vertex_binding_descriptions = shader.get_vertex_input_binding_descriptions();
                vertex_attribute_descriptions = shader.get_vertex_input_attribute_descriptions();
            }

            push_constant_ranges.extend(shader.get_push_constant_ranges().iter());
            descriptor_set_layouts.extend(shader.get_descriptor_set_layouts());
        }

        let vertex_input_state_info = ash::vk::PipelineVertexInputStateCreateInfo::builder()
            .vertex_binding_descriptions(vertex_binding_descriptions)
            .vertex_attribute_descriptions(vertex_attribute_descriptions);

        let vertex_input_assembly_state_info =
            ash::vk::PipelineInputAssemblyStateCreateInfo::builder()
                .topology(self.m_topology)
                .primitive_restart_enable(false);

        let viewports = [*viewport_state.get_viewport()];
        let scissor_rectangles = [*viewport_state.get_scissor_rectangle()];

        let viewport_state_info = ash::vk::PipelineViewportStateCreateInfo::builder()
            .viewports(&viewports)
            .scissors(&scissor_rectangles);

        let dynamic_state_info =
            ash::vk::PipelineDynamicStateCreateInfo::builder().dynamic_states(&[
                ash::vk::DynamicState::VIEWPORT,
                ash::vk::DynamicState::SCISSOR,
            ]);

        let rasterisation_state_info = ash::vk::PipelineRasterizationStateCreateInfo::builder()
            .depth_clamp_enable(self.m_depth_clamp_enable)
            .rasterizer_discard_enable(false)
            .polygon_mode(self.m_polygon_mode)
            .line_width(self.m_line_width)
            .cull_mode(self.m_cull_mode)
            .front_face(self.m_front_face)
            .depth_bias_enable(false);

        let multisample_state_info = ash::vk::PipelineMultisampleStateCreateInfo::builder()
            .sample_shading_enable(false)
            .rasterization_samples(ash::vk::SampleCountFlags::TYPE_1);

        let attachment_states = [ash::vk::PipelineColorBlendAttachmentState::builder()
            .color_write_mask(
                ash::vk::ColorComponentFlags::R
                    | ash::vk::ColorComponentFlags::G
                    | ash::vk::ColorComponentFlags::B
                    | ash::vk::ColorComponentFlags::A,
            )
            .blend_enable(true)
            .alpha_blend_op(ash::vk::BlendOp::ADD)
            .src_alpha_blend_factor(ash::vk::BlendFactor::ONE)
            .dst_alpha_blend_factor(ash::vk::BlendFactor::ZERO)
            .color_blend_op(ash::vk::BlendOp::ADD)
            .src_color_blend_factor(ash::vk::BlendFactor::SRC_ALPHA)
            .dst_color_blend_factor(ash::vk::BlendFactor::ONE_MINUS_SRC_ALPHA)
            .build()];

        let colour_blend_state_info = ash::vk::PipelineColorBlendStateCreateInfo::builder()
            .logic_op_enable(false)
            .attachments(&attachment_states);

        let pipeline_layout_info = ash::vk::PipelineLayoutCreateInfo::builder()
            .push_constant_ranges(&push_constant_ranges)
            .set_layouts(&descriptor_set_layouts);

        let pipeline_layout = unsafe {
            device
                .get_device_handle()
                .create_pipeline_layout(&pipeline_layout_info, None)?
        };

        let depth_stencil_state_info = ash::vk::PipelineDepthStencilStateCreateInfo::builder()
            .depth_test_enable(true)
            .depth_write_enable(true)
            .depth_compare_op(ash::vk::CompareOp::LESS)
            .depth_bounds_test_enable(false)
            .stencil_test_enable(false);

        let mut pipeline_rendering_info = ash::vk::PipelineRenderingCreateInfoKHR::builder()
            .color_attachment_formats(&self.m_colour_formats)
            .depth_attachment_format(self.m_depth_format)
            .stencil_attachment_format(self.m_depth_format);

        let pipeline_info = ash::vk::GraphicsPipelineCreateInfo::builder()
            .push_next(&mut pipeline_rendering_info)
            .stages(&shader_stage_infos)
            .vertex_input_state(&vertex_input_state_info)
            .input_assembly_state(&vertex_input_assembly_state_info)
            .viewport_state(&viewport_state_info)
            .rasterization_state(&rasterisation_state_info)
            .multisample_state(&multisample_state_info)
            .color_blend_state(&colour_blend_state_info)
            .layout(pipeline_layout)
            .dynamic_state(&dynamic_state_info)
            .depth_stencil_state(&depth_stencil_state_info)
            .build();

        let pipeline = unsafe {
            device
                .get_device_handle()
                .create_graphics_pipelines(ash::vk::PipelineCache::null(), &[pipeline_info], None)
                .expect("Pipeline creation failed.")[0]
        };

        Ok(Pipeline {
            m_device: device,
            m_pipeline_handle: pipeline,
            m_pipeline_layout: pipeline_layout,
        })
    }

    pub fn shaders(mut self, shaders: Vec<Rc<Shader>>) -> Self {
        self.m_shaders = shaders;
        self
    }

    pub fn topology(mut self, topology: ash::vk::PrimitiveTopology) -> Self {
        self.m_topology = topology;
        self
    }

    pub fn depth_clamp_enable(mut self, enable: bool) -> Self {
        self.m_depth_clamp_enable = enable;
        self
    }

    pub fn polygon_mode(mut self, polygon_mode: ash::vk::PolygonMode) -> Self {
        self.m_polygon_mode = polygon_mode;
        self
    }

    pub fn line_width(mut self, line_width: f32) -> Self {
        self.m_line_width = line_width;
        self
    }

    pub fn cull_mode(mut self, cull_mode: ash::vk::CullModeFlags) -> Self {
        self.m_cull_mode = cull_mode;
        self
    }

    pub fn front_face(mut self, front_face: ash::vk::FrontFace) -> Self {
        self.m_front_face = front_face;
        self
    }

    pub fn colour_formats(mut self, formats: Vec<ash::vk::Format>) -> Self {
        self.m_colour_formats = formats;
        self
    }

    pub fn depth_format(mut self, format: ash::vk::Format) -> Self {
        self.m_depth_format = format;
        self
    }

    pub fn stencil_format(mut self, format: ash::vk::Format) -> Self {
        self.m_stencil_format = format;
        self
    }
}

impl Default for PipelineBuilder {
    fn default() -> Self {
        Self {
            m_shaders: Vec::new(),
            m_topology: ash::vk::PrimitiveTopology::TRIANGLE_LIST,
            m_depth_clamp_enable: false,
            m_polygon_mode: ash::vk::PolygonMode::FILL,
            m_line_width: 1.0,
            m_cull_mode: ash::vk::CullModeFlags::NONE,
            m_front_face: ash::vk::FrontFace::CLOCKWISE,
            m_colour_formats: Vec::new(),
            m_depth_format: ash::vk::Format::UNDEFINED,
            m_stencil_format: ash::vk::Format::UNDEFINED,
        }
    }
}

pub struct Pipeline {
    m_device: Rc<Device>,
    m_pipeline_handle: ash::vk::Pipeline,
    m_pipeline_layout: ash::vk::PipelineLayout,
}

impl Pipeline {
    pub fn builder() -> PipelineBuilder {
        PipelineBuilder::default()
    }

    pub fn get_pipeline_handle(&self) -> &ash::vk::Pipeline {
        &self.m_pipeline_handle
    }

    pub fn get_layout(&self) -> &ash::vk::PipelineLayout {
        &self.m_pipeline_layout
    }
}

impl Drop for Pipeline {
    fn drop(&mut self) {
        unsafe {
            self.m_device
                .get_device_handle()
                .destroy_pipeline_layout(self.m_pipeline_layout, None);
            self.m_device
                .get_device_handle()
                .destroy_pipeline(self.m_pipeline_handle, None);
        }
    }
}

pub struct ViewportState {
    m_viewport: ash::vk::Viewport,
    m_scissor_rectangle: ash::vk::Rect2D,
}

impl ViewportState {
    pub fn new(width: u32, height: u32) -> Self {
        let mut viewport_state = Self {
            m_viewport: ash::vk::Viewport::default(),
            m_scissor_rectangle: ash::vk::Rect2D::default(),
        };

        viewport_state.update(width, height);
        viewport_state
    }

    pub fn update(&mut self, width: u32, height: u32) {
        self.m_viewport = ash::vk::Viewport::builder()
            .x(0.0)
            .y(height as f32)
            .width(width as f32)
            .height(-(height as f32))
            .min_depth(0.0)
            .max_depth(1.0)
            .build();

        self.m_scissor_rectangle = ash::vk::Rect2D::builder()
            .offset(ash::vk::Offset2D::builder().x(0).y(0).build())
            .extent(
                ash::vk::Extent2D::builder()
                    .width(width)
                    .height(height)
                    .build(),
            )
            .build();
    }

    pub fn get_viewport(&self) -> &ash::vk::Viewport {
        &self.m_viewport
    }

    pub fn get_scissor_rectangle(&self) -> &ash::vk::Rect2D {
        &self.m_scissor_rectangle
    }
}
