use std::{error::Error, rc::Rc};

use crate::*;

pub struct FrameResources {
    m_device: Rc<Device>,

    m_command_pool: ash::vk::CommandPool,
    m_command_buffer: ash::vk::CommandBuffer,

    m_image_acquired_semaphore: ash::vk::Semaphore,
    m_render_finished_semaphore: ash::vk::Semaphore,
    m_render_finished_fence: ash::vk::Fence,
}

impl FrameResources {
    pub fn new(device: Rc<Device>) -> Result<Self, Box<dyn Error>> {
        let command_pool_info = ash::vk::CommandPoolCreateInfo::builder()
            .queue_family_index(device.get_graphics_family_index())
            .flags(ash::vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER);

        let command_pool = unsafe {
            device
                .get_device_handle()
                .create_command_pool(&command_pool_info, None)?
        };

        let command_buffer_info = ash::vk::CommandBufferAllocateInfo::builder()
            .command_pool(command_pool)
            .level(ash::vk::CommandBufferLevel::PRIMARY)
            .command_buffer_count(1);

        let command_buffer = unsafe {
            device
                .get_device_handle()
                .allocate_command_buffers(&command_buffer_info)?[0]
        };

        let image_acquired_semaphore = unsafe {
            device
                .get_device_handle()
                .create_semaphore(&ash::vk::SemaphoreCreateInfo::default(), None)?
        };

        let render_finished_semaphore = unsafe {
            device
                .get_device_handle()
                .create_semaphore(&ash::vk::SemaphoreCreateInfo::default(), None)?
        };

        let render_finished_fence = unsafe {
            device.get_device_handle().create_fence(
                &ash::vk::FenceCreateInfo::builder().flags(ash::vk::FenceCreateFlags::SIGNALED),
                None,
            )?
        };

        Ok(Self {
            m_device: device,

            m_command_pool: command_pool,
            m_command_buffer: command_buffer,

            m_image_acquired_semaphore: image_acquired_semaphore,
            m_render_finished_semaphore: render_finished_semaphore,
            m_render_finished_fence: render_finished_fence,
        })
    }

    pub fn get_command_pool(&self) -> &ash::vk::CommandPool {
        &self.m_command_pool
    }

    pub fn get_command_buffer(&self) -> &ash::vk::CommandBuffer {
        &self.m_command_buffer
    }

    pub fn begin_command_buffer(
        &self,
        usage_flags: ash::vk::CommandBufferUsageFlags,
    ) -> Result<(), Box<dyn Error>> {
        unsafe {
            Ok(self.m_device.get_device_handle().begin_command_buffer(
                self.m_command_buffer,
                &ash::vk::CommandBufferBeginInfo::builder().flags(usage_flags),
            )?)
        }
    }

    pub fn end_command_buffer(&self) -> Result<(), Box<dyn Error>> {
        unsafe {
            Ok(self
                .m_device
                .get_device_handle()
                .end_command_buffer(self.m_command_buffer)?)
        }
    }

    pub fn get_image_acquired_semaphore(&self) -> &ash::vk::Semaphore {
        &self.m_image_acquired_semaphore
    }

    pub fn get_render_finished_semaphore(&self) -> &ash::vk::Semaphore {
        &self.m_render_finished_semaphore
    }

    pub fn get_render_finished_fence(&self) -> &ash::vk::Fence {
        &self.m_render_finished_fence
    }

    pub fn await_render_finished_fence(&self) -> Result<(), Box<dyn Error>> {
        unsafe {
            Ok(self.m_device.get_device_handle().wait_for_fences(
                &[self.m_render_finished_fence],
                true,
                std::u64::MAX,
            )?)
        }
    }

    pub fn reset_render_finished_fence(&self) -> Result<(), Box<dyn Error>> {
        unsafe {
            Ok(self
                .m_device
                .get_device_handle()
                .reset_fences(&[self.m_render_finished_fence])?)
        }
    }

    pub fn reset_command_buffer(&self) -> Result<(), Box<dyn Error>> {
        unsafe {
            Ok(self.m_device.get_device_handle().reset_command_buffer(
                self.m_command_buffer,
                ash::vk::CommandBufferResetFlags::empty(),
            )?)
        }
    }
}

impl Drop for FrameResources {
    fn drop(&mut self) {
        unsafe {
            self.m_device
                .get_device_handle()
                .destroy_semaphore(self.m_image_acquired_semaphore, None);
            self.m_device
                .get_device_handle()
                .destroy_semaphore(self.m_render_finished_semaphore, None);

            self.m_device
                .get_device_handle()
                .destroy_fence(self.m_render_finished_fence, None);

            // This destroys all the command buffers allocated with it, so no explicit destruction is needed.
            self.m_device
                .get_device_handle()
                .destroy_command_pool(self.m_command_pool, None);
        }
    }
}
