use std::{
    error::Error,
    ffi::{c_void, CStr, CString},
};

use crate::util;

pub struct InstanceBuilder {
    m_application_name: String,
    m_application_version: u32,
    m_required_layer_names: Vec<CString>,
    m_window_extension_names: Vec<CString>,
}

impl InstanceBuilder {
    pub fn application_name(mut self, application_name: String) -> InstanceBuilder {
        self.m_application_name = application_name;
        self
    }

    pub fn application_version(mut self, application_version: u32) -> InstanceBuilder {
        self.m_application_version = application_version;
        self
    }

    pub fn required_layer_names(mut self, required_layer_names: Vec<String>) -> InstanceBuilder {
        self.m_required_layer_names = required_layer_names
            .iter()
            .map(|layer| CString::new(layer.clone()).unwrap())
            .collect::<Vec<_>>();
        self
    }

    pub fn window_extension_names(mut self, window_extension_names: Vec<&CStr>) -> InstanceBuilder {
        self.m_window_extension_names = window_extension_names
            .iter()
            .map(|extension| CString::new(extension.to_str().unwrap()).unwrap())
            .collect::<Vec<_>>();
        self
    }

    pub fn build(self) -> Result<Instance, Box<dyn Error>> {
        let entry = unsafe { ash::Entry::load()? };

        let application_name = CString::new(self.m_application_name).unwrap();

        let application_info = ash::vk::ApplicationInfo::builder()
            .application_name(application_name.as_c_str())
            .application_version(self.m_application_version)
            .engine_name(CStr::from_bytes_with_nul(b"Vulkan Engine\0").unwrap())
            .engine_version(ash::vk::make_api_version(0, 1, 0, 0))
            .api_version(ash::vk::make_api_version(0, 1, 2, 0));

        // Check that we support all the requested layers.
        let supported_layers = entry
            .enumerate_instance_layer_properties()?
            .iter()
            .map(|supported_layer| unsafe { CStr::from_ptr(supported_layer.layer_name.as_ptr()) })
            .collect::<Vec<_>>();

        for required_layer in self.m_required_layer_names.iter() {
            if !supported_layers.contains(&required_layer.as_c_str()) {
                return Err(format!(
                    "The layer {:?} was requested but is not supported.",
                    required_layer
                )
                .into());
            }
        }

        // If the application has requested the validation layers, then we should enable the debug utils
        // extension and create a debug messenger.
        let validation_layers_enabled = self
            .m_required_layer_names
            .contains(&CString::new("VK_LAYER_KHRONOS_validation").unwrap());

        // The application should be in charge of requesting the desired layers, but all extensions should be
        // automatically requested as needed.
        let mut required_extension_names: Vec<CString> = self.m_window_extension_names;

        if validation_layers_enabled {
            required_extension_names.push(
                CString::new(ash::extensions::ext::DebugUtils::name().to_str().unwrap()).unwrap(),
            );
        }

        // For use of the VK_KHR_dynamic_rendering device extension.
        required_extension_names.push(
            CString::new(
                ash::extensions::khr::GetPhysicalDeviceProperties2::name()
                    .to_str()
                    .unwrap(),
            )
            .unwrap(),
        );

        // Check that we support all the requested extensions.
        let supported_extensions = entry
            .enumerate_instance_extension_properties(None)?
            .iter()
            .map(|supported_extension| unsafe {
                CStr::from_ptr(supported_extension.extension_name.as_ptr())
            })
            .collect::<Vec<_>>();

        for required_extension in required_extension_names.iter() {
            if !supported_extensions.contains(&required_extension.as_c_str()) {
                return Err(format!(
                    "The instance extension {:?} was requested but is not supported.",
                    required_extension
                )
                .into());
            }
        }

        let mut debug_messenger_info = ash::vk::DebugUtilsMessengerCreateInfoEXT::builder()
            .flags(ash::vk::DebugUtilsMessengerCreateFlagsEXT::empty())
            .message_severity(
                ash::vk::DebugUtilsMessageSeverityFlagsEXT::WARNING
                    | ash::vk::DebugUtilsMessageSeverityFlagsEXT::ERROR,
            )
            .message_type(
                ash::vk::DebugUtilsMessageTypeFlagsEXT::GENERAL
                    | ash::vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION
                    | ash::vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE,
            )
            .pfn_user_callback(Some(debug_callback))
            .user_data(std::ptr::null_mut());

        let converted_layer_names = util::cstring_to_ptr_arr(&self.m_required_layer_names);
        let converted_extension_names = util::cstring_to_ptr_arr(&required_extension_names);

        let instance_info = ash::vk::InstanceCreateInfo::builder()
            .push_next(&mut debug_messenger_info)
            .application_info(&application_info)
            .enabled_layer_names(&converted_layer_names)
            .enabled_extension_names(&converted_extension_names);

        let instance = unsafe { entry.create_instance(&instance_info, None)? };

        log::trace!("Initialised instance.");

        let mut debug_utils_loader = None;
        let mut debug_utils_messenger = None;

        if validation_layers_enabled {
            debug_utils_loader = Some(ash::extensions::ext::DebugUtils::new(&entry, &instance));
            debug_utils_messenger = unsafe {
                Some(
                    debug_utils_loader
                        .as_ref()
                        .unwrap()
                        .create_debug_utils_messenger(&debug_messenger_info, None)?,
                )
            };
        }

        log::trace!("Initialised debug messenger.");

        Ok(Instance {
            m_entry: entry,
            m_instance_handle: instance,
            m_loaded_layer_names: self.m_required_layer_names,
            m_loaded_extension_names: required_extension_names,
            m_debug_utils_loader: debug_utils_loader,
            m_debug_messenger: debug_utils_messenger,
        })
    }
}

impl Default for InstanceBuilder {
    fn default() -> Self {
        Self {
            m_application_name: "".to_string(),
            m_application_version: ash::vk::make_api_version(0, 1, 0, 0),
            m_required_layer_names: Vec::new(),
            m_window_extension_names: Vec::new(),
        }
    }
}

pub struct Instance {
    m_entry: ash::Entry,
    m_instance_handle: ash::Instance,

    m_loaded_layer_names: Vec<CString>,
    m_loaded_extension_names: Vec<CString>,

    m_debug_utils_loader: Option<ash::extensions::ext::DebugUtils>,
    m_debug_messenger: Option<ash::vk::DebugUtilsMessengerEXT>,
}

impl Instance {
    pub fn builder() -> InstanceBuilder {
        InstanceBuilder::default()
    }

    pub fn get_instance_handle(&self) -> &ash::Instance {
        &self.m_instance_handle
    }

    pub fn get_loaded_layer_names(&self) -> Vec<CString> {
        self.m_loaded_layer_names.clone()
    }

    pub fn get_loaded_extension_names(&self) -> Vec<CString> {
        self.m_loaded_extension_names.clone()
    }

    pub fn get_entry(&self) -> &ash::Entry {
        &self.m_entry
    }
}

impl Drop for Instance {
    fn drop(&mut self) {
        unsafe {
            if self.m_debug_utils_loader.is_some() {
                self.m_debug_utils_loader
                    .as_ref()
                    .unwrap()
                    .destroy_debug_utils_messenger(self.m_debug_messenger.unwrap(), None);
                log::trace!("Dropped debug messenger.");
            }

            self.m_instance_handle.destroy_instance(None);
            log::trace!("Dropped instance.");
        }
    }
}

// Custom callback for the validation layers.
unsafe extern "system" fn debug_callback(
    message_flag: ash::vk::DebugUtilsMessageSeverityFlagsEXT,
    message_type: ash::vk::DebugUtilsMessageTypeFlagsEXT,
    p_callback_data: *const ash::vk::DebugUtilsMessengerCallbackDataEXT,
    _: *mut c_void,
) -> ash::vk::Bool32 {
    use ash::vk::DebugUtilsMessageSeverityFlagsEXT as Flag;

    let message = CStr::from_ptr((*p_callback_data).p_message);
    match message_flag {
        Flag::INFO => log::info!("{:?} - {:?}", message_type, message),
        Flag::WARNING => log::warn!("{:?} - {:?}", message_type, message),
        Flag::ERROR => log::error!("{:?} - {:?}", message_type, message),
        _ => (),
    }

    ash::vk::FALSE
}
