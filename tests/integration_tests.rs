use std::rc::Rc;

#[test]
fn test_engine() {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("trace")).init();

    let event_loop = winit::event_loop::EventLoop::new();
    let window = winit::window::WindowBuilder::new()
        .build(&event_loop)
        .unwrap();

    let instance = rendering_engine::Instance::builder()
        .application_name(String::from("New Application"))
        .application_version(ash::vk::make_api_version(0, 1, 0, 0))
        .required_layer_names(vec![
            String::from("VK_LAYER_KHRONOS_validation"),
            String::from("VK_LAYER_MESA_overlay"),
        ])
        .window_extension_names(ash_window::enumerate_required_extensions(&window).unwrap())
        .build()
        .unwrap();

    // We should have the 2 requested layers loaded.
    assert_eq!(2, instance.get_loaded_layer_names().len());

    // We should have at least 1 extension loaded, as requesting the validation layer loads the debug extension.
    assert_ne!(0, instance.get_loaded_extension_names().len());

    let mut surface = rendering_engine::Surface::new(&window, &instance).unwrap();

    let device = Rc::new(rendering_engine::Device::new(&instance, &surface, false).unwrap());

    surface
        .initialise_swapchain(&window, &instance, device)
        .unwrap();
}

// cargo test --workspace -- --nocapture --test-threads=1
